﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DBPOIGET.Classes
{
    public class POIs
    {
        public static Dictionary<string, Poi> poidic = new Dictionary<string, Poi>();
        /// <summary>
        /// 每下载一个POI的事件
        /// </summary>
        public event EventHandler OnePoiDownloaded;

        //当前对象
        POIs pois;
        /// <summary>
        /// 获取POI
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="llng"></param>
        /// <param name="tlat"></param>
        /// <param name="rlng"></param>
        /// <param name="blat"></param>
        /// <param name="ak"></param>
        public void Init(string keyword, double llng, double tlat, double rlng, double blat,string ak,CoordinateType type)
        {
            int pagesize = 10;
            int page = 0;
            while (pois == null || (page + 1) * pagesize < pois.total)
            {
                if (pois != null)
                {
                    if (page * pagesize < pois.total)
                    {
                        page++;
                    }
                }
                string retValue = "";
                try
                {
                    string url = @"http://api.map.baidu.com/place/v2/search?query=" + keyword
                        + "&page_size=" + pagesize + "&page_num=" + page + "&scope=2&bounds="
                        + blat + "," + llng + "," + tlat + "," + rlng + "&output=json&ak=" + ak;
                    CookieCollection cookies = new CookieCollection();
                    HttpWebResponse response = HttpHelper.CreateGetHttpResponse(url, null, null, cookies);
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        retValue = sr.ReadToEnd();
                        sr.Close();
                    }
                }
                catch (Exception)
                {
                    retValue = "{\"status\":\"0\"}";
                }
                if (retValue.Length != 0)
                {
                    var serializer = new JavaScriptSerializer();
                    pois = serializer.Deserialize<POIs>(retValue);
                    for (int i = 0; i < pois.results.Count; i++)
                    {
                        var p = pois.results[i];
                        //去除可能出现的重复POI
                        if (!POIs.poidic.ContainsKey(p.uid))
                        {
                            Coordinate coor = new Coordinate(p.location.lng, p.location.lat);
                            if (type == CoordinateType.GCJ_02)
                            {
                                coor.ToGCJ();
                            }
                            else if (type == CoordinateType.WGS_84)
                            {
                                coor.ToWGS();
                            }
                            p.location.lat = coor.Latitude;
                            p.location.lng = coor.Longitude;
                            POIs.poidic.Add(p.uid, p);
                            if (this.OnePoiDownloaded != null)
                            {
                                this.OnePoiDownloaded(this, new EventArgs());
                            }
                        }
                    }
                }
            }
        }

        public int status { get; set; }

        public string message { get; set; }

        public int total { get; set; }

        public List<Poi> results { get; set; }

        public class Poi
        {
            /// <summary>
            /// poi名称 
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// poi经纬度坐标 
            /// </summary>
            public Loaction location { get; set; }

            /// <summary>
            /// poi地址信息 
            /// </summary>
            public string address { get; set; }

            /// <summary>
            /// poi电话信息 
            /// </summary>
            public string telephone { get; set; }

            /// <summary>
            /// poi的唯一标示 
            /// </summary>
            public string uid { get; set; }

            /// <summary>
            /// 街景图id 
            /// </summary>
            public string street_id { get; set; }

            /// <summary>
            /// 是否有详情页：1有，0没有 
            /// </summary>
            public string detail { get; set; }

            /// <summary>
            /// poi的扩展信息，仅当scope=2时，显示该字段，不同的poi类型，显示的detail_info字段不同
            /// </summary>
            public Detail_Info detail_info { get; set; }
        }

        public class Loaction
        {
            /// <summary>
            /// 纬度值
            /// </summary>
            public double lat { get; set; }

            /// <summary>
            /// 经度值
            /// </summary>
            public double lng { get; set; }
        }

        public class Detail_Info
        {
            /// <summary>
            /// 距离中心点的距离，圆形区域检索时返回 
            /// </summary>
            public int distance { get; set; }

            /// <summary>
            ///所属分类，如’hotel’、’cater’。 
            /// </summary>
            public string type { get; set; }

            /// <summary>
            ///  标签 
            /// </summary>
            public string tag { get; set; }

            /// <summary>
            /// poi的详情页 
            /// </summary>
            public string detail_url { get; set; }

            /// <summary>
            /// poi商户的价格 
            /// </summary>
            public string price { get; set; }

            /// <summary>
            /// 营业时间 
            /// </summary>
            public string shop_hours { get; set; }

            /// <summary>
            /// 总体评分 
            /// </summary>
            public string overall_rating { get; set; }

            /// <summary>
            /// 口味评分 
            /// </summary>
            public string taste_rating { get; set; }

            /// <summary>
            /// 服务评分 
            /// </summary>
            public string service_rating { get; set; }

            /// <summary>
            /// 环境评分 
            /// </summary>
            public string environment_rating { get; set; }

            /// <summary>
            /// 星级（设备）评分 
            /// </summary>
            public string facility_rating { get; set; }

            /// <summary>
            /// 卫生评分 
            /// </summary>
            public string hygiene_rating { get; set; }

            /// <summary>
            /// 技术评分 
            /// </summary>
            public string technology_rating { get; set; }

            /// <summary>
            /// 图片数 
            /// </summary>
            public string image_num { get; set; }

            /// <summary>
            /// 团购数 
            /// </summary>
            public int groupon_num { get; set; }

            /// <summary>
            /// 优惠数 
            /// </summary>
            public int discount_num { get; set; }

            /// <summary>
            /// 评论数 
            /// </summary>
            public string comment_num { get; set; }

            /// <summary>
            /// 收藏数 
            /// </summary>
            public string favorite_num { get; set; }

            /// <summary>
            /// 签到数 
            /// </summary>
            public string checkin_num { get; set; }
        }
        
    }
}
